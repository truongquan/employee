<?php
return [

    'title' => [
        'user.login' => 'common.title.login',
        'home' => 'common.title.top',
        'user.search' => 'common.title.search',
        'user.detail'  => 'common.title.detail',
        'user.create' => 'common.title.add',
        'user.create.conf' => 'common.title.add.confirm',
        'user.create.complete' => 'common.title.add.confirm',
        'user.update' => 'common.title.edit',
        'user.update.conf' => 'common.title.edit.confirm',
        'user.update.complete' => 'common.title.edit.complete',
        'user.delete.conf' => 'common.title.delete.confirm',
        'user.logout' => 'common.title.logout',
    ],

    'heading' => [
        'user.login' => 'common.heading.login',
        'home' => 'common.heading.top',
        'user.search' => 'common.heading.search',
        'user.detail'  => 'common.heading.detail',
        'user.create' => 'common.heading.add',
        'user.create.conf' => 'common.heading.add.confirm',
        'user.create.complete' => 'common.heading.add.confirm',
        'user.update' => 'common.heading.edit',
        'user.update.conf' => 'common.heading.edit.confirm',
        'user.update.complete' => 'common.heading.edit.complete',
        'user.delete.conf' => 'common.heading.delete.confirm',
        'user.logout' => 'common.heading.logout',
    ],

];