<?php
return [
    //page
    'meta-description' => '社員管理システム ログインページです。',
    'heading' => '社員管理システム',
    'title.login' => 'ログイン | 社員管理システム',
    'heading.login' => 'ログイン',
    'title.top' => '社員管理システム',
    'heading.top' => 'トップページ',
    'title.search' => '検索 | 社員管理システム',
    'heading.search' => '検索',
    'title.detail' => '社員管理システム',
    'heading.detail' => 'の詳細',
    'title.add' => '追加 | 社員管理システム',
    'heading.add' => '追加',
    'title.add.confirm' => '追加（確認） | 社員管理システム',
    'heading.add.confirm' => '追加（確認）',
    'title.add.complete' => '追加（完了） | 社員管理システム',
    'heading.add.complete' => '追加（完了）',
    'title.edit' => '編集 | 社員管理システム',
    'heading.edit' => '編集',
    'title.edit.confirm' => '編集（確認） | 社員管理システム',
    'heading.edit.confirm' => '編集（確認）',
    'title.edit.complete' => '編集（完了） | 社員管理システム',
    'heading.edit.complete' => '編集（完了）',
    'title.delete.confirm' => '削除（確認） | 社員管理システム',
    'heading.delete.confirm' => '削除（確認）',
    'title.logout' => 'ログアウト | 社員管理システム',
    'heading.logout' => 'ログアウト',
    'add.complete' => 'として追加しました。',
    'edit.complete' => 'を更新しました。',

    //form element
    'button-submit' => 'ログイン',
    'email' => 'メールアドレス',
    'password' => 'パスワード',

    //message
    'FailedLoginMessage' => 'These credentials do not match any record',
    'noRecordFound' => '検索結果にマッチしませんでした。',
];