@extends('layouts.main')

@section('content')
<section>
{!! Form::open(['url' => '/login', 'class' => 'pure-form']) !!}

@if (in_array(config('app.display_error'), [SHOW_ON_TOP, SHOW_BOTH]))
{!! ErrorDisplay::getInstance()->DisplayAll($errors) !!}
@endif

{!! Form::input_text('email', 'Email', $errors, 'email') !!}
{!! Form::input_text('password', 'Password', $errors, 'password') !!}
<button type="submit" class="pure-button pure-input-1-4 pure-button-primary">{{trans('common.button-submit')}}</button>

{!! Form::close() !!}
</section>
@endsection
