@extends('layouts.main')

@section('content')
<section>
    <p>
        削除しました。
    </p>
    <div>
        <a class="pure-button pure-button-primary" href="{{route('user.search')}}">検索画面へ</a>
    </div>
</section>
@endsection