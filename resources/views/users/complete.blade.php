@extends('layouts.main')

@section('content')
<section>
<p>
    ID：<a href="">{{$user_id}}</a>{{$message}}
</p>
<div>
@if ($login_user->role_id == EMPLOYEE)
    <a class="pure-button pure-button-primary" href="{{route('home')}}">Go back</a>
@else
    <a class="pure-button pure-button-primary" href="{{url('/search')}}">Go to Search Screen</a>
@endif
</div>
</section>
@endsection
