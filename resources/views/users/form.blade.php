@extends('layouts.main')

@section('content')
<section>
{!! Form::model($user, ['route' => $route, 'class' => 'pure-form']) !!}

@if (in_array(config('app.display_error'), [SHOW_ON_TOP, SHOW_BOTH]))
{!! ErrorDisplay::getInstance()->DisplayAll($errors) !!}
@endif

<table class="pure-table pure-table-bordered" width="100%">
    <tbody>
        @if ($id)
        <th>ID</th>
        <td>
            {{$id}}
        </td>
        @endif
        {!! Form::input_cell('name', 'Name', $errors) !!}
        {!! Form::input_cell('kana', 'Kana', $errors) !!}
        @if ($login_user->role_id != EMPLOYEE)
        {!! Form::input_cell('email', 'Email', $errors, 'email') !!}
        {!! Form::input_cell('email_confirmation', 'Email Confirmation', $errors) !!}
        @endif
        {!! Form::input_cell('telephone', 'Telephone', $errors) !!}
        {!! Form::input_cell('birthday', 'Birthday', $errors, 'text', 'mm/dd/yyyy') !!}
        @if ($login_user->role_id != EMPLOYEE)
        {!! Form::input_cell('note', 'Note', $errors, 'textarea') !!}
        @endif
        {!! Form::input_cell('password', 'Password', $errors, 'password') !!}
        @if ($login_user->role_id == ADMIN)
        {!! Form::select_cell('role_id', 'Role', $role_list, $errors) !!}
        {!! Form::select_cell('boss_id', 'Boss', $boss_list, $errors) !!}
        @elseif ($login_user->role_id == BOSS)
        {!! Form::hidden('role_id', EMPLOYEE) !!}
        {!! Form::hidden('boss_id', $login_user->id) !!}
        @endif
        <tr>
            <td colspan="2" align="right">
            @if ($login_user->role_id == EMPLOYEE)
                <a class="pure-button pure-button-primary" href="{{route('home')}}">Back</a>
            @else
                <a class="pure-button pure-button-primary" href="{{route('user.search')}}">Go to Search Screen</a>
            @endif
                <button class="pure-button button-error" name="submit" type="submit">Confirm</button>
            </td>
        </tr>
    </tbody>
</table>
{!! Form::close() !!}
</section>
@endsection
