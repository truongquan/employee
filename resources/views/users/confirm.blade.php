@extends('layouts.main')

@section('content')
<section>
<table class="pure-table pure-table-bordered" width="100%">
    <tbody>
        @if (!is_null($id))
        <tr>
            <th>ID</th>
            <td>
                {{$id}}               
            </td>
        </tr>
        @endif
        <tr>
            <th>Name</th>
            <td>
                {{$name}}               
            </td>
        </tr>
        <tr>
            <th>Kana</th>
            <td>
                {{$kana}}            
            </td>
        </tr>
        @if ($login_user->role_id != EMPLOYEE)
        <tr>
            <th>Email</th>
            <td>
                {{$email}}    
            </td>
        </tr>
        @endif
        <tr>
            <th>Telephone</th>
            <td>
                {{$telephone}}               
            </td>
        </tr>
        <tr>
            <th>Birthday</th>
            <td>
                {{$birthday}}              
            </td>
        </tr>
        @if ($login_user->role_id != EMPLOYEE)
        <tr>
            <th>Note</th>
            <td>
                {!! nl2br($note) !!}                
            </td>
        </tr>
        @endif

        @if ($login_user->role_id == ADMIN)
        <tr>
            <th>Role</th>
            <td>
                {{$role_list[$role_id]}}               
            </td>
        </tr>
        <tr>
            <th>BOSS</th>
            <td>
                @if (isset($boss_id))
                {{$boss_list[$boss_id]}}               
                @endif
            </td>
        </tr>
        @endif

        <tr>

            <td align="right">
            @if (isset($route_back))

                {!! Form::open(['route' => $route_back]) !!}
                    {!! Form::hidden('id', $id) !!}
                    {!! Form::hidden('name', $name) !!}
                    {!! Form::hidden('kana', $kana) !!}
                    @if ($login_user->role_id != EMPLOYEE)
                    {!! Form::hidden('email', $email) !!}
                    {!! Form::hidden('email_confirmation', $email) !!}
                    @endif
                    {!! Form::hidden('password', $password) !!}
                    {!! Form::hidden('telephone', $telephone) !!}
                    {!! Form::hidden('birthday', $birthday) !!}
                    @if ($login_user->role_id != EMPLOYEE)
                    {!! Form::hidden('note', $note) !!}
                    {!! Form::hidden('role_id', $role_id) !!}
                    {!! Form::hidden('boss_id', $boss_id) !!}
                    @endif
                    <button class="pure-button pure-button-primary" type="submit">Back</button>
                {!! Form::close() !!}

            @else
                <a href="{{route('user.detail', $id)}}" class="pure-button pure-button-primary">Back</a>
            @endif
            </td>
            <td align="left">
                {!! Form::open(['route' => $route]) !!}
                    {!! Form::hidden('name', $name) !!}
                    {!! Form::hidden('kana', $kana) !!}
                    @if ($login_user->role_id != EMPLOYEE)
                    {!! Form::hidden('email', $email) !!}
                    {!! Form::hidden('email_confirmation', $email) !!}
                    @endif
                    {!! Form::hidden('password', $password) !!}
                    {!! Form::hidden('telephone', $telephone) !!}
                    {!! Form::hidden('birthday', $birthday) !!}
                    @if ($login_user->role_id != EMPLOYEE)
                    {!! Form::hidden('note', nl2br($note)) !!}
                    {!! Form::hidden('role_id', $role_id) !!}
                    {!! Form::hidden('boss_id', $boss_id) !!}
                    @endif
                    <button class="pure-button button-error"  type="submit">Continue</button>
                {!! Form::close() !!}
            </td>
        </tr>
    </tbody>
</table>
</section>
@endsection
