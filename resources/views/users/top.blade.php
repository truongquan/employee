@extends('layouts.main')

@section('content')

    <nav class="pure-menu pure-menu-horizontal">
        {!! (new \Prototype\Pagination\Presenter($users))->render() !!}
    </nav>

    <section>
        <table class="pure-table pure-table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>名前</th>
                    <th>メールアドレス</th>
                    <th>電話番号</th>
                    <th>生年月日</th>
                    <th>更新日時</th>
                    @if ($login_user->role_id == ADMIN)
                    <th>権限</th>
                    @endif
                </tr>
            </thead>
            <tbody>
            @if (!$users->isEmpty())
            {{--*/ $i = 1; /*--}}
            @foreach($users as $user)
            {{--*/ $class = ($i%2 == 0) ? '' : 'pure-table-odd'; $i++; /*--}}
                <tr class="{{$class}}">
                    <td>{{$user->id}}</td>
                    <td><a href="{{route('user.detail', $user->id)}}">{{$user->name.' '.$user->kana}}</a></td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->telephone}}</td>
                    <td>{{$user->birthday}}</td>
                    <td>{{$user->updated_at}}</td>
                    @if ($login_user->role_id == ADMIN)
                    <td>{{$role_list[$user->role_id]}}</td>
                    @endif
                </tr>
            @endforeach
            @endif
            </tbody>
        </table>
    </section>

    <nav class="pure-menu pure-menu-horizontal">
       {!! (new \Prototype\Pagination\Presenter($users))->render() !!}
    </nav>
@endsection