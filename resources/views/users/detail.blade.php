@extends('layouts.main')

@section('content')
<section>
<table class="pure-table pure-table-bordered" width="100%">
    <tbody>
        <tr>
            <th>ID</th>
            <td>{{$user->id}}</td>
        </tr>
        <tr>
            <th>名前</th>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <th>名前（カナ）</th>
            <td>{{$user->kana}}</td>
        </tr>
        <tr>
            <th>メールアドレス</th>
            <td>{{$user->email}}</td>
        </tr>
        <tr>
            <th>電話番号</th>
            <td>{{$user->telephone}}</td>
        </tr>
        <tr>
            <th>生年月日</th>
            <td>{{$user->birthday}}</td>
        </tr>

        @if ($login_user->role_id != EMPLOYEE)
            <tr>
                <th>ノート</th>
                <td>
                {!! $user->note !!}
                </td>
            </tr>
        @endif

        @if ($login_user->role_id == ADMIN)
            <tr>
                <th>権限</th>
                <td>{{$role_list[$user->role_id]}}</td>
            </tr>
            <tr>
                <th>BOSS</th>
                <td>{{$boss_list[$user->boss_id]}}</td>
            </tr>
        @endif

        <tr>
            <th>更新日時</th>
            <td>{{$user->updated_at}}</td>
        </tr>
        <tr>
            <td colspan="2" align="right">
            @if ($login_user->role_id != EMPLOYEE)
                {!! Form::open(['route' => ['user.delete.conf', $user->id]]) !!}
                    {!! Form::hidden('name', $user->name) !!}
                    {!! Form::hidden('kana', $user->kana) !!}
                    {!! Form::hidden('email', $user->email) !!}
                    {!! Form::hidden('email_confirmation', $user->email) !!}
                    {!! Form::hidden('password', $user->password) !!}
                    {!! Form::hidden('telephone', $user->telephone) !!}
                    {!! Form::hidden('birthday', $user->birthday) !!}
                    {!! Form::hidden('note', $user->note) !!}
                    {!! Form::hidden('role_id', $user->role_id) !!}
                    {!! Form::hidden('boss_id', $user->boss_id) !!}
                    {!! Form::hidden('is_delete', 1) !!}
                    <a class="pure-button pure-button-primary" href="{{route('user.search')}}">Go to Search Screen</a>
                    <a class="pure-button button-secondary" href="{{route('user.update', $user->id)}}">Edit</a>
                    <button type="submit" class="pure-button button-error">Delete</button>
                {!! Form::close() !!}
            @else
                <a class="pure-button button-secondary" href="{{route('user.update', $user->id)}}">Edit</a>
            @endif
            </td>
        </tr>
    </tbody>
</table>
</section>
@endsection
