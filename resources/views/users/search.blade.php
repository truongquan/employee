@extends('layouts.main')

@section('content')

        <section>
        {!! Form::open(['route' => 'user.search', 'method' => 'get', 'class' => 'pure-form']) !!}
        {!! ErrorDisplay::getInstance()->DisplayAll($errors) !!}
        <table class="pure-table pure-table-bordered">
            <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! Form::text('name', old('name')) !!}</td>
                    <td>Email</td>
                    <td>{!! Form::email('email', old('email')) !!}</td>
                </tr>
                <tr>
                    <td>Kana</td>
                    <td>{!! Form::text('kana', old('kana')) !!}</td>
                    <td>Telephone</td>
                    <td>{!! Form::text('telephone', old('telephone')) !!}</td>
                </tr>
                <tr>
                    <td>Birthday</td>
                    <td colspan="3">
                        {!! Form::text('start_date', old('start_date'), ['placeholder' => 'Start Date']) !!}
                        &nbsp;～&nbsp;
                        {!! Form::text('end_date', old('end_date'), ['placeholder' => 'End Date']) !!}
                    </td>
                </tr>
                @if ($login_user->role_id == ADMIN)
                <tr>
                    <td>権限</td>
                    <td colspan="3" align="center">
                        <ul class="pure-menu-list pure-menu-horizontal">
                            <li class="pure-menu-item pure-u-1-6"><label for="admin">{!! Form::checkbox('admin') !!}管理者</label></li>
                            <li class="pure-menu-item pure-u-1-6"><label for="boss">{!! Form::checkbox('boss') !!}BOSS</label></li>
                            <li class="pure-menu-item pure-u-1-6"><label for="employee">{!! Form::checkbox('employee') !!}従業員</label></li>
                        </ul>
                    </td>
                </tr>
                @endif
                <tr>
                    <td colspan="4" align="right">
                        <button class="pure-button pure-button-primary" type="submit">検索</button>
                    </td>
                </tr>
            </tbody>
        </table>
        {!! Form::close() !!}
    </section>
    <nav class="pure-menu pure-menu-horizontal">
        {!! (new \Prototype\Pagination\Presenter($users))->render() !!}
    </nav>

    <section>
        @if (!$users->isEmpty())
        <table class="pure-table pure-table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>名前</th>
                    <th>メールアドレス</th>
                    <th>電話番号</th>
                    <th>生年月日</th>
                    <th>更新日時</th>
                    @if ($login_user->role_id == ADMIN)
                    <th>権限</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                {{--*/ $i = 1; /*--}}
                @foreach($users as $user)
                {{--*/ $class = ($i%2 == 0) ? '' : 'pure-table-odd'; $i++; /*--}}
                <tr class="{{$class}}">
                    <td>{{$user->id}}</td>
                    <td><a href="{{url('/member/'.$user->id.'/detail')}}">{{$user->name.' ('.$user->kana.')'}}</a></td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->telephone}}</td>
                    <td>{{$user->birthday}}</td>
                    <td>{{$user->updated_at}}</td>
                    @if ($login_user->role_id == ADMIN)
                    <td>{{$role_list[$user->role_id]}}</td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <p class="error-box">
        {{trans('common.noRecordFound')}}
        </p>
        @endif
    </section>

    <nav class="pure-menu pure-menu-horizontal">
        {!! (new \Prototype\Pagination\Presenter($users))->render() !!}
    </nav>
</section>
@endsection