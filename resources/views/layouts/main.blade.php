<!DOCTYPE html>
<html lang="ja">
<head>
    <meta content="{{trans('common.meta-description')}}" name="description">
    <title>
        @if ($current_route == 'user.detail')
            {{$login_user->name.' ('.$login_user->kana.') | '.trans($title)}}
        @else
            {{trans($title)}}
        @endif
    </title>
    <link rel="stylesheet" href="{{url('css/pure-min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{url('css/custom.css')}}" type="text/css" />
</head>
<body>

<header>
    <nav class="home-menu pure-menu pure-menu-horizontal relative">
        <h1 class="pure-menu-heading"><a href="{{url('/')}}">{{trans('common.heading')}}</a></h1>
        @if (isset($login_user))
        <ul class="pure-menu-list force-right">
            <li class="pure-menu-item"><span class="pure-menu-link">{{$login_user->name}}</span></li>
            @if ($login_user->role_id == BOSS || $login_user->role_id == ADMIN)
            <li class="pure-menu-item"><a href="{{route('user.search')}}" class="pure-menu-link">Search</a></li>
            <li class="pure-menu-item"><a href="{{url(route('user.create'))}}" class="pure-menu-link">Add</a></li>
            @endif
            <li class="pure-menu-item"><a href="{{url('/logout')}}" class="pure-menu-link">Logout</a></li>
        </ul>
        @endif
    </nav>
</header>

<section class="contents">
    <h2>
    @if ($current_route == 'user.detail')
    {{$login_user->name.' ('.$login_user->kana.') '.trans($heading)}}
    @else
    {{trans($heading)}}
    @endif
    </h2>

    @yield('content')

</section>

<footer>
Employer Management System
</footer>

</body>
</html>
