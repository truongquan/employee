<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('boss_id')->nullable();
			$table->integer('role_id');
			$table->string('name')->nullable();
			$table->string('kana')->nullable();
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('telephone')->nullable();
			$table->dateTime('birthday')->nullable();
			$table->softDeletes();
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
