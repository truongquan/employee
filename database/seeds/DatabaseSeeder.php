<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('UserTableSeeder');
		DB::table('roles')->delete();
		DB::table('users')->delete();

		$roles = [
			ADMIN => 'Administrator',
			BOSS => 'Boss',
			EMPLOYEE => 'Employee'
		];
		$data = array();
		foreach ($roles as $key => $value) {
			$data[] = ['id' => $key, 'name' => $value]; 
		}

		DB::table('roles')->insert($data);

		$names = ['Quan', 'Thang', 'Tao', 'Lan', 'Tuan', 'Tung', 'Phuong', 'Son']; 
		$kanas = ['Quan kana', 'Thang kana', 'Tao kana', 'Lan kana', 'Tuan kana', 'Tung kana', 'Phuong kana', 'Son kana'];
		$birthdays = ['1996-02-23', '1995-09-11', '1994-12-01', '1987-03-09', '1988-12-28', '1986-07-30'];
		$telephones = ['097-989-991', '089-121-892', '920-238-232', '293-239-928'];

		//admin
		for ($i = 1; $i <=10; $i++) {
			DB::table('users')->insert([
				'id' => $i,
				'boss_id' => 0,
				'role_id' => ADMIN,
				'name'	=> $names[array_rand($names)],
				'kana' => $kanas[array_rand($kanas)],
				'email' => 'admin'.$i.'@gmail.com',
				'birthday' => $birthdays[array_rand($birthdays)],
				'telephone' => $telephones[array_rand($telephones)],
				'password' => bcrypt('12345678'),
			]);
		}

		//boss
		for ($i = 11; $i <=20; $i++) {
			DB::table('users')->insert([
				'id' => $i,
				'boss_id' => 0,
				'role_id' => BOSS,
				'name'	=> $names[array_rand($names)],
				'kana' => $kanas[array_rand($kanas)],
				'email' => 'boss'.$i.'@gmail.com',
				'birthday' => $birthdays[array_rand($birthdays)],
				'telephone' => $telephones[array_rand($telephones)],
				'password' => bcrypt('12345678'),
			]);
		}

		//employee
		for ($i = 21; $i <=30; $i++) {
			DB::table('users')->insert([
				'id' => $i,
				'boss_id' => rand(11, 20),
				'role_id' => EMPLOYEE,
				'name'	=> $names[array_rand($names)],
				'kana' => $kanas[array_rand($kanas)],
				'email' => 'employee'.$i.'@gmail.com',
				'birthday' => $birthdays[array_rand($birthdays)],
				'telephone' => $telephones[array_rand($telephones)],
				'password' => bcrypt('12345678'),
			]);
		}
		
	}

}
