<?php namespace Prototype\DBClasses\Eloquent;

use Illuminate\Contracts\Auth\Guard;
use Prototype\Interfaces\UserInterface;
use App\User;
use App\Note;
use App\Role;
use Session;

class EloquentUserRepository implements UserInterface
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getList($role_id, $id, $limit = PER_PAGE)
    {
        if ($role_id == ADMIN) {
            $users = $this->model->orderBy('updated_at', 'desc')->paginate($limit);
        } elseif ($role_id == BOSS) {
            $users = $this->model->where('boss_id', $id)
                                 ->orWhere('id', $id)
                                 ->orderBy('updated_at', 'desc')
                                 ->paginate($limit);
        }
        

        return $users->setPath(route('home'));
    }

    public function formModify($data, $id = null)
    {
        if ($id) {
            $user = $this->model->find($id);
            $note = is_null($user->note) ? new Note : $user->note;
        } else {
            $user = $this->model;
            $note = new Note;
        }

        $data['birthday'] = date('Y-m-d H:i:s', strtotime($data['birthday']));
        $data['password'] = bcrypt($data['password']);
        $user->fill($data);
        $user->save();

        if (isset($data['note'])) {
            $note->user_id = $user->id;
            $note->note = $data['note'];
            $note->save();
        }

        return $user;
    }

    public function search($data, $role_id, $id, $limit = PER_PAGE)
    {
        $query = $this->model;

        if (isset($data['name']) && trim($data['name']) !== '') {
            $query = $query->where('name', 'like', $data['name'].'%');
        }

        if (isset($data['kana']) && trim($data['kana']) !== '') {
            if ($query instanceof User) {
                $query = $query->where('kana', 'like', $data['kana'].'%');
            } else {
                $query->where('kana', 'like', $data['kana'].'%');
            }
        }

        if (isset($data['email']) && trim($data['email']) !== '') {
            if ($query instanceof User) {
                $query = $query->where('email', $data['email']);
            } else {
                $query->where('email', $data['email']);
            }
        }

        if (isset($data['telephone']) && trim($data['telephone']) !== '') {
            if ($query instanceof User) {
                $query = $query->where('telephone', 'like', $data['telephone'].'%');
            } else {
                $query->where('telephone', 'like', $data['telephone'].'%');
            }
        }

        if (isset($data['start_date']) && trim($data['start_date']) !== '') {
            if ($query instanceof User) {
                $query = $query->where('birthday', '>=', $data['start_date']);
            } else {
                $query->where('birthday', '>=', $data['start_date']);
            }
        }

        if (isset($data['end_date']) && trim($data['end_date']) !== '') {
            if ($query instanceof User) {
                $query = $query->where('birthday', '<=', $data['end_date']);
            } else {
                $query->where('birthday', '<=', $data['end_date']);
            }
        }

        if ($role_id == BOSS) {
            if ($query instanceof User) {
                $query = $query->where('boss_id', $id)
                               ->orWhere('id', $id);
            } else {
                $query->where('boss_id', $id)
                      ->orWhere('id', $id);
            }
        }

        $query = $query->paginate($limit);

        return $query->setPath(route('user.search'))->appends($data);
    }

    public function getById($id, $with = array())
    {
        $user = $this->model->with($with)->findOrFail($id);

        if (!is_null($user->note)) {
            $user->note = $user->note->note;
        }

        return $user;
    }

    public function getListIdsByRole($role)
    {
        $users = $this->model->select('id', 'name')
                             ->where('role_id', $role)
                             ->get();

        if (is_null($users)) {

            return false;
        }

        $result = [0 => '-'];
        foreach ($users as $value) {
            $result[$value->id] = $value->name;
        }

        return $result;   
    }

    public function getListRoles()
    {
        $roles = Role::all();

        $result = [];
        foreach ($roles as $value) {
            $result[$value->id] = $value->name;
        }

        return $result;
    }

    public function delete($id)
    {
        $user = $this->model->find($id);
        $user->note()->delete();

        return $user->delete();
    }
}
