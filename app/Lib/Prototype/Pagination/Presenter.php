<?php namespace Prototype\Pagination;

use Illuminate\Contracts\Pagination\Paginator as PaginatorContract;
use Prototype\Pagination\CustomUrlWindow as UrlWindow;
use Illuminate\Pagination\UrlWindowPresenterTrait;
use Prototype\Pagination\PresenterHTML;

class Presenter extends PresenterHTML
{

    use UrlWindowPresenterTrait;
    
    public function __construct(PaginatorContract $paginator, UrlWindow $window = null)
    {
        $this->paginator = $paginator;
        $this->window = is_null($window) ? UrlWindow::make($paginator) : $window->get();
    }

    /**
     * Determine if the underlying paginator being presented has pages to show.
     *
     * @return bool
     */
    public function hasPages()
    {
        return $this->paginator->hasPages();
    }

    /**
     * Convert the URL window into Pagination HTML.
     *
     * @return string
     */
    public function render()
    {
        if ($this->hasPages())
        {
            //dd($this->getLastButton());
            return sprintf(
                $this->getPaginationWrapperHTML(),
                $this->getFirstButton(),
                $this->getPreviousButton(),
                $this->getLinks(),
                $this->getNextButton(),
                $this->getLastButton()
            );
        }

        return '';
    }

    protected function getLastButton()
    {
        // If the current page is greater than or equal to the last page, it means we
        // can't go any further into the pages, as we're already on this last page
        // that is available, so we will make it the "next" link style disabled.
        if (!$this->paginator->hasMorePages()) {
            return $this->getDisabledTextWrapper($this->getLastButtonText());
        }

        $url = $this->paginator->url($this->paginator->lastPage());

        return $this->getPageLinkWrapper($url, $this->getLastButtonText());
    }

    protected function getFirstButton()
    {
        // If the current page is less than or equal to one, it means we can't go any
        // further back in the pages, so we will render a disabled previous button
        // when that is the case. Otherwise, we will give it an active "status".
        if ($this->paginator->currentPage() <= 1) {
            return $this->getDisabledTextWrapper($this->getFirstButtonText());
        }

        $url = $this->paginator->url(1);

        return $this->getPageLinkWrapper($url, $this->getFirstButtonText());
    }
    /**
     * Get HTML wrapper for an available page link.
     *
     * @param  string $url
     * @param  int $page
     * @return string
     */
    protected function getAvailablePageWrapper($url, $page)
    {
        return sprintf($this->getAvailablePageWrapperHTML(),$url, $page);
    }

    /**
     * Get HTML wrapper for disabled text.
     *
     * @param  string  $text
     * @return string
     */
    protected function getDisabledTextWrapper($text)
    {
        return sprintf($this->getDisabledPageWrapperHTML(), $text);
    }

    /**
     * Get HTML wrapper for active text.
     *
     * @param  string  $text
     * @return string
     */
    protected function getActivePageWrapper($text)
    {
        return sprintf($this->getActivePageWrapperHTML(), $text);
    }

    /**
     * Get a pagination "dot" element.
     *
     * @return string
     */
    protected function getDots()
    {
        return $this->getDisabledTextWrapper($this->getDotsText());
    }

    /**
     * Get the current page from the paginator.
     *
     * @return int
     */
    protected function currentPage()
    {
        return $this->paginator->currentPage();
    }

    /**
     * Get the last page from the paginator.
     *
     * @return int
     */
    protected function lastPage()
    {
        return $this->paginator->lastPage();
    }

    /**
     * Get HTML wrapper for a page link.
     *
     * @param  string $url
     * @param  int $page
     * @return string
     */
    protected function getPageLinkWrapper($url, $page)
    {
        if ($page == $this->paginator->currentPage())
        {
            return $this->getActivePageWrapper($page);
        }

        return $this->getAvailablePageWrapper($url, $page);
    }

    /**
     * Get the previous page pagination element.
     *
     * @return string
     */
    protected function getPreviousButton()
    {
        // If the current page is less than or equal to one, it means we can't go any
        // further back in the pages, so we will render a disabled previous button
        // when that is the case. Otherwise, we will give it an active "status".
        if ($this->paginator->currentPage() <= 1) {
            return $this->getDisabledTextWrapper($this->getPreviousButtonText());
        }

        $url = $this->paginator->url(
            $this->paginator->currentPage() - 1
        );

        return $this->getPageLinkWrapper($url, $this->getPreviousButtonText());
    }

    /**
     * Get the next page pagination element.
     *
     * @return string
     */
    protected function getNextButton()
    {
        // If the current page is greater than or equal to the last page, it means we
        // can't go any further into the pages, as we're already on this last page
        // that is available, so we will make it the "next" link style disabled.
        if (!$this->paginator->hasMorePages()) {
            return $this->getDisabledTextWrapper($this->getNextButtonText());
        }

        $url = $this->paginator->url($this->paginator->currentPage() + 1);

        return $this->getPageLinkWrapper($url, $this->getNextButtonText());
    }
}