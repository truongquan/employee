<?php namespace Prototype\Pagination;

class PresenterHTML
{
    /**
     * Pagination wrapper HTML.
     *
     * @var string
     */
    protected $paginationWrapper = '<ul class="pure-menu-list">%s %s %s %s %s</ul>';
    /**
     * Available page wrapper HTML.
     *
     * @var string
     */
    protected $availablePageWrapper = '<li class="pure-menu-item"><a href="%s" class="pure-menu-link pure-button">%s</a></li>';
    /**
     * Get active page wrapper HTML.
     *
     * @var string
     */
    protected $activePageWrapper = '<li class="pure-menu-item"><button class="pure-button pure-button-disabled">%s</button></li>';
    /**
     * Get disabled page wrapper HTML.
     *
     * @var string
     */
    protected $disabledPageWrapper = '<li class="pure-menu-item"><button class="pure-button pure-button-disabled">%s</button></li>';
     /**
     * Previous button text.
     *
     * @var string
     */
    protected $previousButtonText = 'back';
    /**
     * Next button text.
     *
     * @var string
     */
    protected $nextButtonText = 'next';
    /***
     * "Dots" text.
     *
     * @var string
     */
    protected $dotsText = '...';

    protected $lastButtonText = 'last';

    protected $firstButtonText = 'first';

    /**
     * Get pagination wrapper HTML.
     *
     * @return string
     */
    protected function getPaginationWrapperHTML() {
        return $this->paginationWrapper;
    }

    /**
     * Get available page wrapper HTML.
     *
     * @return string
     */
    protected function getAvailablePageWrapperHTML() {
        return $this->availablePageWrapper;
    }

    /**
     * Get active page wrapper HTML.
     *
     * @return string
     */
    protected function getActivePageWrapperHTML() {
        return $this->activePageWrapper;
    }

    /**
     * Get disabled page wrapper HTML.
     *
     * @return string
     */
    protected function getDisabledPageWrapperHTML() {
        return $this->disabledPageWrapper;
    }

    /**
     * Get previous button text.
     *
     * @return string
     */
    protected function getPreviousButtonText() {
        return $this->previousButtonText;
    }

    /**
     * Get next button text.
     *
     * @return string
     */
    protected function getNextButtonText() {
        return $this->nextButtonText;
    }

    /***
     * Get "dots" text.
     *
     * @return string
     */
    protected function getDotsText() {
        return $this->dotsText;
    }

    protected function getLastButtonText()
    {
        return $this->lastButtonText;
    }

    protected function getFirstButtonText()
    {
        return $this->firstButtonText;
    }

}