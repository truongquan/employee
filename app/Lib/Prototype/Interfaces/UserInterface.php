<?php
namespace Prototype\Interfaces;

interface UserInterface {
    public function getList($role_id, $id, $limit = PER_PAGE); //get list of users limit by role_id
    public function formModify($data, $id = null); //create a new user or update an existing user
    public function getById($id, $with = array()); //get an user by id and it relation
    public function getListIdsByRole($role); //get list ids and names of users by user role
    public function getListRoles(); //get list ids and names of user roles
    public function search($data, $role_id, $id, $limit = PER_PAGE); //search user by data[$field] => $value
    public function delete($id);
}
