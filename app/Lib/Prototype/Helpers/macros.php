<?php
Form::macro('input_text', function($name, $label, $errors, $type = 'text') {
    $html = '<fieldset class="pure-group">';
    $option = ['placeholder' => $label];
    $html .= Form::$type($name, null, $option);

    if (in_array(config('app.display_error'), [SHOW_INDIVIDUAL, SHOW_BOTH])) {
        $html .= ErrorDisplay::getInstance()->DisplayIndividual($errors, $name);
    }
    
    $html .= '</fieldset>';

    return $html;
});

Form::macro('input_cell', function($name, $label, $errors, $type = 'text', $help = '') {
    $option = ['class' => 'pure-input-1'];
    $error_html = '';
    $class = '';

    if (in_array(config('app.display_error'), [SHOW_INDIVIDUAL, SHOW_BOTH])) {
        $error_html = ErrorDisplay::getInstance()->DisplayIndividual($errors, $name);
        $class = ($error_html === false) ? '' : 'error-cell';
    }

    $html = '<tr>';
    $html .= '<th class="'.$class.'">'.$label.'</th>';
    $html .= '<td>'.Form::$type($name, null, $option).$help;
    $html .= $error_html.'</td>';
    $html .= '</tr>';

    return $html;
});

Form::macro('select_cell', function($name, $label, $value = array(), $errors) {
    $option = ['class' => 'pure-input-1'];
    $error_html = '';
    $class = '';
    if (in_array(config('app.display_error'), [SHOW_INDIVIDUAL, SHOW_BOTH])) {
        $error_html = ErrorDisplay::getInstance()->DisplayIndividual($errors, $name);
        $class = ($error_html === false) ? '' : 'error-cell';
    }

    $html = '<tr>';
    $html .= '<th class="'.$class.'">'.$label.'</th>';
    $html .= '<td>'.Form::select($name, $value, null, $option);
    $html .= $error_html.'</td>';
    $html .= '</tr>';

    return $html;
});