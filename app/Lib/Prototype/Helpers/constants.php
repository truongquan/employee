<?php
//define user role
define('ADMIN', 1);
define('BOSS', 2);
define('EMPLOYEE', 3);

//define authenticate redirect link
define('REDIRECT_PATH', '/');
define('LOGIN_PATH', '/login');
define('LOGOUT_PATH', '/logout');

//define error display type
define('SHOW_ON_TOP', 1);
define('SHOW_INDIVIDUAL', 2);
define('SHOW_BOTH', 3);

define('PER_PAGE', 5);
define('NUM_LINKS', 3);