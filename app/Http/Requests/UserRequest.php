<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::has('is_delete')) {

            return [];
        }

        $rules = [
                'password'          =>  'required|min:8',
                'name'              =>  'max:16',
                'kana'              =>  'max:16',
                'telephone'         =>  'max:13',
                'birthday'          =>  'date',
            ];

        if (\Auth::user()->role_id == EMPLOYEE) {
            $rules['email'] = 'email|confirmed|unique:users';
        }else {
            $rules['email'] = 'required|email|confirmed|unique:users';
        }

        return $rules;
    }
}
