<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SearchRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'max:16',
            'email'      => 'email|max:254',
            'kana'       => 'max:16',
            'telephone'  => 'max:13',
            'start_date' => 'date_format:Y-m-d',
            'end_date'   => 'date_format:Y-m-d|after:start_date',
        ];
    }
}
