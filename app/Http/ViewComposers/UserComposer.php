<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Prototype\Interfaces\UserInterface as UserRepository;
use Route;

class UserComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $user;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $user)
    {
        // Dependencies automatically resolved by service container...
        $this->user = $user;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data['role_list'] = $this->user->getListRoles();
        $data['boss_list'] = $this->user->getListIdsByRole(BOSS);
        $data['login_user'] = \Auth::user();

        $data['current_route'] = Route::currentRouteName();

        $titles = config('header.title');
        $data['title'] = $titles[$data['current_route']];

        $headings = config('header.heading');
        $data['heading'] = $headings[$data['current_route']];

        $view->with($data);
    }

}