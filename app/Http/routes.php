<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/login', ['uses' => 'Auth\AuthController@getLogin', 'as' => 'user.login']);
Route::post('/login', 'Auth\AuthController@postLogin');
Route::get('/logout', ['uses' => 'Auth\AuthController@getLogout', 'as' => 'user.logout']);
Route::get('/add', ['uses' => 'UsersController@form', 'as' => 'user.create']);
Route::post('/add/conf', ['uses' => 'UsersController@confirm', 'as' => 'user.create.conf']);
Route::post('/add/comp', ['uses' => 'UsersController@complete', 'as' => 'user.create.complete']);
Route::get('/member/{id}/edit', ['uses' => 'UsersController@form', 'as' => 'user.update']);
Route::post('/member/{id}/edit/conf', ['uses' => 'UsersController@confirm', 'as' => 'user.update.conf']);
Route::post('/member/{id}/edit/comp', ['uses' => 'UsersController@complete', 'as' => 'user.update.complete']);
Route::post('/back/{id?}', ['uses' => 'UsersController@back', 'as' => 'user.back']);
Route::get('/search', ['uses' => 'UsersController@search', 'as' => 'user.search']);
Route::get('/member/{id}/detail', ['uses' => 'UsersController@show', 'as' => 'user.detail']);
Route::post('/member/{id}/delete/conf', ['uses' => 'UsersController@confirm', 'as' => 'user.delete.conf']);
Route::post('/member/{id}/delete/comp', ['uses' => 'UsersController@destroy', 'as' => 'user.delete.complete']);

Route::get('/', ['uses' => 'UsersController@index', 'as' => 'home']);
