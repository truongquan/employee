<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Prototype\Interfaces\UserInterface as UserRepository;
use App\Http\Requests\UserRequest;
use App\Http\Requests\SearchRequest;

use Input;

class UsersController extends Controller
{
	protected $repo;
    protected $user_login;

	public function __construct(UserRepository $repo)
	{
		$this->middleware('auth');

		$this->repo = $repo;
        $this->user_login = \Auth::user();
	}

	public function index()
	{
        if ($this->user_login->role_id == ADMIN || $this->user_login->role_id == BOSS) {
            $users = $this->repo->getList($this->user_login->role_id, $this->user_login->id);

            return view('users.top')->with('users', $users);    
        }

        if ($this->user_login->role_id == EMPLOYEE) {

            return $this->show($this->user_login->id);
        }
        
        return view('errors.503');
	}

	public function form($id = null)
    {
        if ($id) {
            $user = $this->repo->getById($id, ['note']);
            $route = ['user.update.conf', $id];
        } else {
            $user = $this->repo;
            $route = 'user.create.conf';
        }

        return view('users.form', compact('user', 'id', 'route'));
    }

    public function confirm(UserRequest $request, $id = null)
    {
        if ($id) {
            if (Input::has('is_delete')) {
                $data['route'] = ['user.delete.complete', $id];
            } else {
                $data['route'] = ['user.update.complete', $id];
                $data['route_back'] = ['user.back', $id];
            }
        } else {
            $data['route'] = 'user.create.complete';
            $data['route_back'] = 'user.back';
        }

        $data['id'] = $id;

    	return view('users.confirm')->with(Input::except('_token'))
    								->with($data);
    }

    public function complete(UserRequest $request, $id = null)
    {
        $user = $this->repo->formModify(Input::except('_token'), $id);
        if ($id) {
            $data['message'] = trans('common.edit.complete');
        } else {
            $data['message'] = trans('common.add.complete');
        }
        $data['user_id'] = $user->id;

        return view('users.complete')->with($data);
    }

    public function back($id = null)
    {
        if ($id) {

            return redirect()->route('user.update', [$id])->withInput();
        } else {

            return redirect()->route('user.create')->withInput();
        }
    }

    public function search(SearchRequest $request)
    {
        if ($this->user_login->role_id == ADMIN || $this->user_login->role_id == BOSS) {
            $users = $this->repo->search(Input::all(), $this->user_login->role_id, $this->user_login->id);

            return view('users.search', compact('users'));
        }

        if ($this->user_login->role_id == EMPLOYEE) {

            return $this->show($this->user_login->id);
        }
        
        return view('errors.503');
    }
	
	public function show($id)
	{
		$user = $this->repo->getById($id, ['note']);

        return view('users.detail', compact('user', 'id'));
	}

	public function destroy($id)
	{
        $this->repo->delete($id);

        return view('users.delete-complete');
	}
}
